# EASY

# Define a method that returns an array of only the even numbers in its argument
# (an array of integers).
def get_evens(arr)
  newarr = []
  arr.each {|ele| newarr << ele if ele.even?}
  newarr

  #Could simply to one line with:
  # arr.select { |ele| ele.even? }
end

# Define a method that returns a new array of all the elements in its argument
# doubled. This method should *not* modify the original array.
def calculate_doubles(arr)
  arr.map { |ele| ele * 2 }
end

# Define a method that returns its argument with all the argument's elements
# doubled. This method should modify the original array.
def calculate_doubles!(arr)
  arr.map! { |ele| ele * 2 }
end

# Define a method that returns the sum of each element in its argument
# multiplied by its index. array_sum_with_index([2, 9, 7]) => 23 because (2 * 0) +
# (9 * 1) + (7 * 2) = 0 + 9 + 14 = 23
def array_sum_with_index(arr)
  sum = 0
  arr.each_with_index { |ele, idx| sum += ele * idx }
  sum
end

# MEDIUM

# Given an array of bids and an actual retail price, return the bid closest to
# the actual retail price without going over that price. Assume there is always
# at least one bid below the retail price.
def price_is_right(bids, actual_retail_price)
  #Tried to figure out how to use reduce to solve this, but got stuck
  highest = 0

  bids.each do |current_bid|
    highest = current_bid if current_bid > highest && current_bid <= actual_retail_price
  end

  highest

  #Could've made a lot simplier by getting rid of the bids that are >= to the actual price
  #Then just picking the highest one
  # lower_bids_than_actual = bids.reject {|bid| bid > actual_retail_price}
  # lower_bids_than_actual.max
end

# Given an array of numbers, return an array of those numbers that have at least
# n factors (including 1 and the number itself as factors).
# at_least_n_factors([1, 3, 10, 16], 5) => [16] because 16 has five factors (1,
# 2, 4, 8, 16) and the others have fewer than five factors. Consider writing a
# helper method num_factors
def at_least_n_factors(numbers, n)
  newarr = []
  numbers.each {|num| newarr << num if num_factors(num) >= n}
  newarr

  #Same thing as the first method, I could simply this three liner into a one liner using select
  # numbers.select {|num| num_factors(num) >= n}
end

def num_factors(number)
  factors = 0
  number.times {|ele| factors += 1 if number % (ele +1) == 0}
  factors

  #Could iterate and count at the same time using count:
  # (1..number).count { |n| number % n == 0}
end

# HARD

# Define a method that accepts an array of words and returns an array of those
# words whose vowels appear in order. You may wish to write a helper method:
# ordered_vowel_word?
def ordered_vowel_words(words)
  newarr = []
  words.each do |w|
    newarr << w if w.downcase.count('aeiou') == 0 || ordered_vowel_word?(w)
  end
  newarr

  #Could have made this much simplier:
  # words.select {|word| ordered_vowel_word?(word)}
end

def ordered_vowel_word?(word)
  vowels = 'a'

  word.downcase.each_char do |char|
    if char.count("aeiou") == 1
      if vowels <= char
        vowels = char
      else
        return false
      end
    end
  end

  true

  #Could have made this much simplier:
  # vowels = "aeiou"
  # vowels_in_word = word.chars.select {|letter| vowels.include?(letter)}
  # vowels_in_word == vowels_in_word.sort
end

# Given an array of numbers, return an array of all the products remaining when
# each element is removed from the array. You may wish to write a helper method:
# array_product.

# products_except_me([2, 3, 4]) => [12, 8, 6], where: 12 because you take out 2,
# leaving 3 * 4. 8, because you take out 3, leaving 2 * 4. 6, because you take out
# 4, leaving 2 * 3

# products_except_me([1, 2, 3, 5]) => [30, 15, 10, 6], where: 30 because you
# take out 1, leaving 2 * 3 * 5 15, because you take out 2, leaving 1 * 3 * 5
# 10, because you take out 3, leaving 1 * 2 * 5 6, because you take out 5,
# leaving 1 * 2 * 3
def products_except_me(numbers)
  newarr = []

  numbers.each_with_index do |num, idx|
    testarr = numbers[0..idx-1] + numbers[idx+1..-1] if idx != 0
    testarr = numbers[1..-1] if idx == 0
    newarr << array_product(testarr)
  end

  newarr

  #Could've used a slightly more advanced method to make this much shorter code:
  # numbers.map.with_index do |num, idx| 
  #   sub_array = numbers[0...idx] + numbers[(idx+1)..-1]
  #   array_product(sub_array)
  # end
end

def array_product(array)
  product = 1
  array.each { |ele| product *= ele }
  product

  #Could have made it a one liner with reduce:
  # array.reduce(:*)
end
