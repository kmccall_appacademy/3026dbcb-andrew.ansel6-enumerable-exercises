require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  #A simple arr.reduce(:+) won't work because it will throw an error when the array is empty
  arr.reduce(0) {|acc,el| acc + el }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    return false unless sub_string?(string, substring)
  end
  true

  #Dang it, could've made this easier on myself by just using .all? and then wouldn't have needed the helper method
  # long_strings.all? {|string| string.include?(substring)}
end

def sub_string?(string1, string2)
  return true if string1.downcase.include?(string2.downcase)
  false
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  newarr = []
  ('a'..'z').each do |char|
    newarr << char if string.count(char) > 1
  end
  newarr

  #Again, start using select, it'll make it much easier:
  # characters = string.chars.uniq
  # characters.delete(" ")
  # characters.select {|char| string.count(char)>1}
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest_two_words_arr = []
  words = string.split
  words.each do |w|
    if longest_two_words_arr.length < 2
      longest_two_words_arr << w
      longest_two_words_arr.sort! {|x,y| x.length <=> y.length}
    elsif w.length > longest_two_words_arr[0].length
      longest_two_words_arr << w
      longest_two_words_arr.shift
      longest_two_words_arr.sort! {|x,y| x.length <=> y.length}
    end
  end
  longest_two_words_arr

  #Wow, complete overthink here..
  string.delete!(",.;:!?")
  string.split.sort_by {|word| word.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  newarr = []
  ('a'..'z').each do |char|
    newarr << char unless string.include?(char)
  end
  newarr

  #A slightly simplier way would be to just use reject instead of building the array myself
  # alphabet = ('a'..'z')
  # alphabet.reject { |ele| string.downcase.include?(ele)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  arr = []
  arr << first_yr if not_repeat_year?(first_yr)
  (last_yr-first_yr).times do |i|
    testyr = first_yr + i + 1
    arr << (testyr) if not_repeat_year?(testyr)
  end
  arr

  #Could've made it easier on myself.. START thinking about select, reject, etc. use everything at your disposal
  # (first_yr..last_yr).select { |yr| no_repeat_years?(year)}
end

def not_repeat_year?(year)
  year_string = year.to_s
  year_string.each_char {|c| return false if year_string.count(c) > 1}
  true
  #Should've just used uniq to help me
  # year_digits = year.to_S.chars
  # year_digits == year_digits.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.each {|song| songs.delete(song) unless no_repeats?(song, songs)}
  songs.uniq

  #Again, could've used select as well:
  # uniq_songs = songs.uniq
  # uniq_songs.select { |song| no_repeats?(song, songs)}
end

def no_repeats?(song_name, songs)
  songs.each_index do |index|
    if songs[index] == song_name && songs[index+1] == song_name
      return false
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  closest_to_end = ""
  words = string.split
  words.each do |w|
    if c_distance(remove_punctuation(w)) != nil
      if c_distance(closest_to_end) == nil
        closest_to_end = remove_punctuation(w)
      elsif c_distance(remove_punctuation(w)) < c_distance(closest_to_end)
        closest_to_end = remove_punctuation(w)
      end
    end
  end
  closest_to_end

  #Using their version of c_distance
  # remove_punctuation(string)
  # c_words = string.split.select { |word| word.downcase.include?('c')}
  # return "" if c_words.empty?
  # c_words.sort_by { |word| c_distance(word)}.first
end

def c_distance(word)
  distance_idx = nil
  word.each_char.with_index do |c,i|
    distance_idx = i if c.downcase == 'c' && (distance_idx == nil || i > distance_idx)
  end
  distance_idx

  #There one liner way:
  # word.reverse.index('c')
end

def remove_punctuation(string)
  alphabet = ('a'..'z').to_a.join
  string.each_char {|c| string.delete!(c) if c.count(alphabet) == 0}
  string

  #Coudl've also done it the opposite way, but I was unsure which punctuationt to throw out:
  # string.delete!(',.;:!?')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  start_idx = 0
  end_idx = 0
  newarr = []

  arr.each_with_index do |value, index|
    i = index #Silly initialization, but I want to manipulate i
    if arr[i] == arr[i+1] && i >= end_idx
      start_idx = i
      until arr[i] != arr[i+1]
        end_idx = i + 1
        i += 1
      end
      newarr << [start_idx, end_idx]
    end
  end

  newarr

  #There version:
  # ranges = []
  # start_index = nil
  #
  # #start walking
  # #set the start_index when we're at the beginning of a range
  # #when we reach the end of a range, add the range to the list and reset the start_index
  #
  # numbers.each_with_index do |el,idx|
  #   next_el = numbers[idx + 1]
  #   if el == next_el
  #     start_index = idx unless start_index #i.e., reset the start_index if it's nil
  #   elsif start_index # i.e., if the start index isn't nil (the numbers switched)
  #     ranges.push([start_index, idx])
  #     start_index = nil # reset the start_index to nil so we can capture more ranges
  #   end
  # end
  #
  # ranges
end
